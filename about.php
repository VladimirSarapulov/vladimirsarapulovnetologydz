<?php 
$name = "Вова";
$town = "Черемхово Иркутская обл";
$my_mail = "svlsitoff@gmail.com";
$age = 32;
$my_self = "слесарь по ремонту подвижного состава";

?>




<!DOCTYPE>
<html lang="ru">
    <head>
        <title><?php echo "$name - $my_self";?></title>
        <meta charset="utf-8">
        <style>
            body {
                font-family: sans-serif;  
            }
            
            dl {
                display: table-row;
            }
            
            dt, dd {
                display: table-cell;
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h1>Страница пользователя <?=$name ?></h1>
        <dl>
            <dt>Имя</dt>
            <dd><?=$name ?></dd>
        </dl>
        <dl>
            <dt>Возраст</dt>
            <dd><?=$age ?></dd>
        </dl>
        <dl>
            <dt>Адрес электронной почты</dt>
            <dd><a href="<?=$my_mail ?>"><?=$my_mail ?></a></dd>
        </dl>
        <dl>
            <dt>Город</dt>
            <dd><?=$town ?></dd>
        </dl>
        <dl>
            <dt>О себе</dt>
            <dd><?=$my_self ?></dd>
        </dl>
    </body>